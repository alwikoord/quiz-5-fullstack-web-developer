var vue_app = new Vue({
    el: '#app',
    data() {
        return {
            perPage: 3,
            currentPage: 1,
            fields: [
                'nama_lengkap',
                'domisili',
                { key: 'actions', label: 'Actions' }
            ],
            items: [
                { nama_lengkap: 'Sudarsono', domisili: 'Surabaya' },
                { nama_lengkap: 'Ahmad', domisili: 'Semarang' }
            ],
            form: {
                nama_lengkap: '',
                domisili: '',
            },
            show: true,
            form_action: 'Insert',
            form_index: 0,
        }
    },
    computed: {
        rows() {
            return this.items.length
        }
    },
    methods: {
        info(item, index, button) {
            alert(JSON.stringify(item, null, 2))
        },
        edit(item, index, button) {
            alert(JSON.stringify(item, null, 2))
            this.form.nama_lengkap = item.nama_lengkap
            this.form.domisili = item.domisili
            this.form_action = 'Update'
            this.form_index = index + ((this.currentPage - 1) * this.perPage)
        },
        del(item, index, button) {
            this.items.splice(index + ((this.currentPage - 1) * this.perPage), 1)
        },
        onSubmit(evt) {
            evt.preventDefault()
            alert(JSON.stringify(this.form))
            if (this.form_action == 'Update') {
                this.items[this.form_index].nama_lengkap = this.form.nama_lengkap
                this.items[this.form_index].domisili = this.form.domisili
            } else {
                this.items.push({ nama_lengkap: this.form.nama_lengkap, domisili: this.form.domisili })
            }
            this.form.nama_lengkap = ''
            this.form.domisili = ''
            this.form_action = 'Insert'
        },
        onReset(evt) {
            evt.preventDefault()
            this.form.nama_lengkap = ''
            this.form.domisili = ''
            this.form_action = 'Insert'
            this.show = false
            this.$nextTick(() => {
                this.show = true
            })
        }
    }
});